/**
 * View Models used by Spring MVC REST controllers.
 */
package labs.training.web.rest.vm;
